import {useState, useEffect} from 'react';
import {Row} from 'react-bootstrap'
import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	// checks to see if the mock data is captured
	// console.log(coursesData);
	// console.log(coursesData[0]);
	
	const [courses, setCourses] = useState([]);

		useEffect(() => {
		fetch('https://afternoon-hollows-43165.herokuapp.com/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const coursesArr = (data.map(course => {

				return (
					<CourseCard key={course._id} courseProp={course} breakpoint={4} />
				)
				
			}))
			setCourses(coursesArr)
		})

	}, [courses])


	// const courses = coursesData.map(course => {
	// 	console.log(course);
	// 	return(
	// 		<CourseCard key={course.id} courseProp={course}/>
	// 	)
	// });

	return(

		<>
		<Row>
			<h1>Available Courses:</h1>
			{courses}
		</Row>
		</>
	)
}

// process of passing through the props
// from the Courses Page -> CourseCard = PHP Laravel
// from CourseCard -> props === parameter

// courseProp from Courses Page ===  props parameter from CourseCard
